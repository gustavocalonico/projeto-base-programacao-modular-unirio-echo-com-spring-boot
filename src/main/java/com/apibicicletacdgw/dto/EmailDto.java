package com.apibicicletacdgw.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class EmailDto {

    @javax.validation.constraints.Email
    @NotBlank
    private String email;

    @NotEmpty
    private String mensagem;
}
