package com.apibicicletacdgw.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Getter
@Setter
public class NewCobrancaDto {

    @NotBlank
    @DecimalMin(value = "0.00", inclusive = false)
    @Digits(integer = 255, fraction = 2)
    private Double valor;

    @NotEmpty
    private UUID ciclista;
}
