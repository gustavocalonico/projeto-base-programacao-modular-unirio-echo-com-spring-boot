package com.apibicicletacdgw.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Getter
@Setter
public class CartaoDeCreditoDto {
    @NotEmpty
    private String cvv;

    @NotEmpty
    private String numero;

    @NotEmpty
    private String nomeTitular;

    @NotEmpty
    @JsonFormat(pattern="MM-yy")
    private Date validade;
}