package com.apibicicletacdgw.repository;

import com.apibicicletacdgw.model.Cobranca;
import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope("singleton")
@Getter
public class FilaCobranca {
    private List<Cobranca> cobrancas;

    public Cobranca save(Cobranca cobranca) {
        cobrancas.add(cobranca);
        return cobranca;
    }
}
