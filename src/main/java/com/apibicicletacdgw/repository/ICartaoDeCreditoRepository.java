package com.apibicicletacdgw.repository;

import com.apibicicletacdgw.model.CartaoDeCredito;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@SuppressWarnings("unused")
public interface ICartaoDeCreditoRepository extends JpaRepository<CartaoDeCredito, UUID> {
}
