package com.apibicicletacdgw.repository;

import com.apibicicletacdgw.model.Cobranca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@SuppressWarnings("unused")
public interface ICobrancaRepository extends JpaRepository<Cobranca, UUID> {
}
