package com.apibicicletacdgw.repository;

import com.apibicicletacdgw.model.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@SuppressWarnings("unused")
public interface IEmailRepository extends JpaRepository<Email, UUID> {
}
