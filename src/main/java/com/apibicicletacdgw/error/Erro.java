package com.apibicicletacdgw.error;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.UUID;

@Getter
public class Erro extends Exception {
    private final UUID id;
    private final int codigo;
    private final String mensagem;

    public Erro(HttpStatus codigo, String mensagem) {
        this.id = UUID.randomUUID();
        this.codigo = codigo.value();
        this.mensagem = mensagem;
    }

}