package com.apibicicletacdgw.controller;

import com.apibicicletacdgw.dto.CartaoDeCreditoDto;
import com.apibicicletacdgw.dto.EmailDto;
import com.apibicicletacdgw.dto.NewCobrancaDto;
import com.apibicicletacdgw.model.CartaoDeCredito;
import com.apibicicletacdgw.model.Cobranca;
import com.apibicicletacdgw.model.Email;
import com.apibicicletacdgw.service.ICartaoService;
import com.apibicicletacdgw.service.IEmailService;
import com.apibicicletacdgw.service.IExternoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.mail.EmailException;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * API Aluguel Bicicletas - Projeto base para a disciplina de Programação Modular.
 *
 * Os métodos da API Aluguel Bicicleta são documentados utilizando a especificação OpenAPI (Swagger) e podem ser visualizados
 * acessando a URL do domínio + "/swagger-ui.html".
 * Acesso em servidor local: http://localhost:8080/swagger-ui.html
 */

@RestController
@RequestMapping("/")
@Tag(name="Spring Boot REST API Bicicleta")
public class ExternoController {

    private final IExternoService externoService;
    private final IEmailService emailService;
    private final ICartaoService cartaoService;
    private final ModelMapper mapper;

    public ExternoController(IExternoService externoService,
                             IEmailService emailService,
                             ICartaoService cartaoService,
                             ModelMapper mapper) {
        this.externoService = externoService;
        this.emailService = emailService;
        this.mapper = mapper;
        this.cartaoService = cartaoService;
    }

    @GetMapping("/")
    @Operation(summary="Retorna informação sobre a API")
    public String getRoot () {
        return "Endpoint raiz externo das bicicletas.";
    }

    @PostMapping("/enviarEmail")
    @Operation(summary="Notificar via email")
    public String postSentMail (@RequestBody EmailDto emailDto) {
        Email email = mapper.map(emailDto, Email.class);

        try {
            emailService.send(email);
            emailService.insertMail(email);
        } catch (EmailException e) {
            return e.getMessage();
        }

        return "Externo solicitada";
    }

    @PostMapping("/cobranca")
    @Operation(summary="Realizar cobrança")
    public Cobranca postCharge (@RequestBody NewCobrancaDto newCobrancaDto) {
        return externoService.insertCobranca(mapper.map(newCobrancaDto, Cobranca.class));
    }

    @PostMapping("/filaCobranca")
    @Operation(summary="Inclui cobrança na fila de cobrança. Cobranças na fila serão cobradas de tempos em tempos.")
    public Cobranca postChargeQueue (@RequestBody NewCobrancaDto newCobrancaDto) {
        return externoService.insertOnCobrancaQueue(mapper.map(newCobrancaDto, Cobranca.class));
    }

    @GetMapping("/cobranca/{idCobranca}")
    @Operation(summary="Obter cobrança")
    public Cobranca getChargeById(@PathVariable UUID idCobranca) {
        return externoService.getCobrancaById(idCobranca);
    }

    @PostMapping("/validaCartaoDeCredito")
    @Operation(summary="Valida um cartão de crédito")
    public ResponseEntity<String> postValidateCreditCard (@RequestBody CartaoDeCreditoDto cartaoDeCreditoDto) {
        CartaoDeCredito cartao = mapper.map(cartaoDeCreditoDto, CartaoDeCredito.class);

        cartaoService.creditOperatorValidation(cartao);
        cartaoService.insertCartaoDeCredito(cartao);

        return ResponseEntity.status(200).body("Dados Atualizados");
    }
}