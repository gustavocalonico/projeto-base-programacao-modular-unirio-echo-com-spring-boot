package com.apibicicletacdgw.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CartaoDeCredito {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @EqualsAndHashCode.Include
    protected UUID id;

    @NotEmpty
    @Pattern(regexp = "\\d{3,4}")
    private String cvv;

    @NotEmpty
    @Pattern(regexp = "\\d+")
    private String numero;

    @NotEmpty
    private String nomeTitular;

    @NotEmpty
    @JsonFormat(pattern="MM-yy")
    private Date validade;
}
