package com.apibicicletacdgw.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Cobranca {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @EqualsAndHashCode.Include
    protected UUID id;

    @NotBlank
    private UUID ciclista;

    @NotBlank
    @JsonFormat(pattern="yyyy-MM-dd HH-mm-ss")
    @CreationTimestamp
    private LocalDateTime horaSolicitacao;

    @NotBlank
    @JsonFormat(pattern="yyyy-MM-dd HH-mm-ss")
    @UpdateTimestamp
    private LocalDateTime horaFinalizacao;

    @NotBlank
    @DecimalMin(value = "0.00", inclusive = false)
    @Digits(integer = 255, fraction = 2)
    private Double valor;

    @NotBlank
    private Status status;

    @SuppressWarnings("unused")
    public enum Status {
        PENDENTE, PAGA, FALHA, CANCELADA, OCUPADA
    }

}
