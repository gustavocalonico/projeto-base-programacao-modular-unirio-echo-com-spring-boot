package com.apibicicletacdgw.service;

import com.apibicicletacdgw.model.Cobranca;
import com.apibicicletacdgw.repository.FilaCobranca;
import com.apibicicletacdgw.repository.ICobrancaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class ExternoService implements IExternoService {

    private final ICobrancaRepository cobrancaDao;
    private final FilaCobranca filaCobranca;

    public Cobranca insertCobranca(Cobranca cobranca) {
        return cobrancaDao.save(cobranca);
    }

    public Cobranca insertOnCobrancaQueue(Cobranca cobranca) {
        insertCobranca(cobranca);
        return filaCobranca.save(cobranca);
    }

    public Cobranca getCobrancaById(UUID cobrancaId) {
        return cobrancaDao.getById(cobrancaId);
    }
}