package com.apibicicletacdgw.service;

import com.apibicicletacdgw.model.CartaoDeCredito;
import com.apibicicletacdgw.repository.ICartaoDeCreditoRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.validator.routines.CreditCardValidator;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CartaoService implements ICartaoService {

    private final ICartaoDeCreditoRepository cartaoDeCreditoDao;

    @Override
    public boolean creditOperatorValidation(CartaoDeCredito cartao) {
        return validate(cartao);
        // return chamada assíncrona para API do operador de cartão de crédito
    }

    private boolean validate(CartaoDeCredito cartao) {
        CreditCardValidator ccv = new CreditCardValidator(CreditCardValidator.MASTERCARD + CreditCardValidator.VISA);
        ccv.validate(cartao.getNumero());
        ccv.validate(cartao.getCvv());
        ccv.validate(String.valueOf(cartao.getValidade()));
        return true;
    }

    public CartaoDeCredito insertCartaoDeCredito(CartaoDeCredito cartaoDeCredito) {
        return cartaoDeCreditoDao.save(cartaoDeCredito);
    }
}
