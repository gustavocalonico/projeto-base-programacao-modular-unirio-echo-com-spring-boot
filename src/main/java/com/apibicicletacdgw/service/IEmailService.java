package com.apibicicletacdgw.service;

import com.apibicicletacdgw.model.Email;
import org.apache.commons.mail.EmailException;

public interface IEmailService {
    boolean send(Email email) throws EmailException;
    Email insertMail(Email email);
}