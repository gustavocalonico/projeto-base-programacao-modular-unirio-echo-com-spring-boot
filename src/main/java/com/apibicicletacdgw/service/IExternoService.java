package com.apibicicletacdgw.service;

import com.apibicicletacdgw.model.Cobranca;

import java.util.UUID;

public interface IExternoService {
    Cobranca insertCobranca(Cobranca cobranca);
    Cobranca insertOnCobrancaQueue(Cobranca cobranca);
    Cobranca getCobrancaById(UUID cobrancaId);
}
