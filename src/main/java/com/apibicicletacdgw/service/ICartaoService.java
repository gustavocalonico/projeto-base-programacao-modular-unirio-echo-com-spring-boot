package com.apibicicletacdgw.service;

import com.apibicicletacdgw.model.CartaoDeCredito;

public interface ICartaoService {
    boolean creditOperatorValidation(CartaoDeCredito cartao);
    CartaoDeCredito insertCartaoDeCredito(CartaoDeCredito cartaoDeCredito);
}