package com.apibicicletacdgw.service;

import com.apibicicletacdgw.model.Email;
import com.apibicicletacdgw.repository.IEmailRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmailService implements IEmailService {

    private final IEmailRepository emailDao;

    @Override
    public boolean send(Email email) throws EmailException {

        SimpleEmail simpleEmail = new SimpleEmail();

        simpleEmail.setHostName("smtp.googlemail.com");
        simpleEmail.setSmtpPort(465);

        simpleEmail.setAuthenticator(new DefaultAuthenticator("pmgus20212@gmail.com", "xg&$^58eS43ASw&YC*q7"));
        simpleEmail.setSSLOnConnect(true);
        simpleEmail.setSSLCheckServerIdentity(true);

        simpleEmail.setFrom("pmgus20212@gmail.com");
        simpleEmail.setSubject("Email de PM");
        simpleEmail.setMsg(email.getMensagem());
        simpleEmail.addTo(email.getEmailAddress());

        simpleEmail.send();

        return true;
    }

    @Override
    public Email insertMail(Email email) {
        return emailDao.save(email);
    }
}
