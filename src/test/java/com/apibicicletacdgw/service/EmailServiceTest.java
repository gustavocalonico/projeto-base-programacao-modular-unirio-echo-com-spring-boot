package com.apibicicletacdgw.service;

import com.apibicicletacdgw.factory.ModelFactory;
import com.apibicicletacdgw.model.Email;
import com.apibicicletacdgw.repository.IEmailRepository;
import org.apache.commons.mail.EmailException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
class EmailServiceTest {

    private IEmailService emailService;
    private IEmailService spy;
    @Mock IEmailRepository emailDao;

    @BeforeEach
    void beforeTest() {
        emailService = new EmailService(emailDao);
        spy = Mockito.spy(emailService);
    }

    @Test
    void testSend() throws EmailException {
        Email email = ModelFactory.createEmail();

        assertTrue(emailService.send(email));
    }

    @Test
    void testInsertEmail() {
        Email email = ModelFactory.createEmail();

        when(emailDao.save(email)).thenReturn(email);

        Email emailInsert = emailService.insertMail(email);

        verify(emailDao, times(1)).save(email);
        assertEquals(emailInsert, email);
    }
}
