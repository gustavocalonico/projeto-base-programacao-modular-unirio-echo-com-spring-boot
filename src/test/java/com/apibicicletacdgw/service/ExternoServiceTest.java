package com.apibicicletacdgw.service;

import com.apibicicletacdgw.error.Erro;
import com.apibicicletacdgw.factory.ModelFactory;
import com.apibicicletacdgw.model.Cobranca;
import com.apibicicletacdgw.repository.FilaCobranca;
import com.apibicicletacdgw.repository.ICobrancaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class ExternoServiceTest {

    private IExternoService externoService;
    @Mock ICobrancaRepository cobrancaDao;
    @Mock FilaCobranca filaCobranca;

    @BeforeEach
    void beforeTest() {
        externoService = new ExternoService(cobrancaDao, filaCobranca);
    }

    @Test
    void testInsertCobranca() {
        Cobranca cobranca = ModelFactory.createCobranca();

        when(cobrancaDao.save(cobranca)).thenReturn(cobranca);

        Cobranca cobrancaInsert = externoService.insertCobranca(cobranca);

        verify(cobrancaDao, times(1)).save(cobranca);
        assertEquals(cobrancaInsert, cobranca);
    }

    @Test
    void testInsertCobrancaQueue() {
        Cobranca cobranca = ModelFactory.createCobranca();

        when(filaCobranca.save(cobranca)).thenReturn(cobranca);

        Cobranca cobrancaInsert = externoService.insertOnCobrancaQueue(cobranca);

        verify(filaCobranca, times(1)).save(cobranca);
        assertEquals(cobrancaInsert, cobranca);
    }

    @Test
    void testGetCobrancaById() {
        UUID uuid = UUID.randomUUID();
        Cobranca cobranca = ModelFactory.createCobranca();
        cobranca.setId(uuid);

        when(cobrancaDao.getById(uuid)).thenReturn(cobranca);

        Cobranca cobrancaInsert = externoService.getCobrancaById(uuid);

        verify(cobrancaDao, times(1)).getById(uuid);
        assertEquals(cobrancaInsert.getId(), cobranca.getId());
        assertEquals(cobrancaInsert, cobranca);
    }

    @Test
    void testGetCobrancaById_CobrancaDoesNotExists_Error() {
        UUID uuid = UUID.randomUUID();
        Cobranca cobranca = ModelFactory.createCobranca();
        cobranca.setId(uuid);

        when(cobrancaDao.getById(uuid)).thenThrow(new EntityNotFoundException());

        try {
            externoService.getCobrancaById(uuid);
        } catch (Exception ex) {
            Erro erro = new Erro(HttpStatus.NOT_FOUND, ex.getMessage());
            verify(cobrancaDao, times(1)).getById(uuid);
            assertEquals(EntityNotFoundException.class, ex.getClass());
            assertEquals(erro.getMensagem(), ex.getMessage());
        }
    }
}
