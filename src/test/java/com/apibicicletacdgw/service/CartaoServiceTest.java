package com.apibicicletacdgw.service;

import com.apibicicletacdgw.factory.ModelFactory;
import com.apibicicletacdgw.model.CartaoDeCredito;
import com.apibicicletacdgw.repository.ICartaoDeCreditoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
class CartaoServiceTest {

    private ICartaoService cartaoService;
    @Mock ICartaoDeCreditoRepository cartaoDeCreditoDao;

    @BeforeEach
    void beforeTest() {
        cartaoService = new CartaoService(cartaoDeCreditoDao);
    }

    @Test
    void testOperatorValidation() {
        CartaoDeCredito cartao = ModelFactory.createCartao();

        assertTrue(cartaoService.creditOperatorValidation(cartao));
    }

    @Test
    void testInsertCartaoDeCredito() {
        CartaoDeCredito cartao = ModelFactory.createCartao();

        when(cartaoDeCreditoDao.save(cartao)).thenReturn(cartao);

        cartaoService.insertCartaoDeCredito(cartao);

        verify(cartaoDeCreditoDao, times(1)).save(cartao);
    }
}
