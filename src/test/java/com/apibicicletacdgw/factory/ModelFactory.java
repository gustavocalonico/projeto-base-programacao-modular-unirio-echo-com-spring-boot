package com.apibicicletacdgw.factory;

import com.apibicicletacdgw.model.CartaoDeCredito;
import com.apibicicletacdgw.model.Cobranca;
import com.apibicicletacdgw.model.Email;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

public class ModelFactory {

    public static CartaoDeCredito createCartao() {
        CartaoDeCredito cartao = new CartaoDeCredito();

        cartao.setNomeTitular("Abigail Teste");
        cartao.setNumero("5364924853672442");
        cartao.setCvv("532");
        cartao.setValidade(new Date());

        return cartao;
    }

    public static Cobranca createCobranca() {
        Cobranca cobranca = new Cobranca();

        cobranca.setCiclista(UUID.fromString("b4abc109-3507-4d7b-94af-b4c6af40f8de"));
        cobranca.setHoraSolicitacao(LocalDateTime.now());
        cobranca.setValor(199.99);

        return cobranca;
    }

    public static Email createEmail() {
        Email email = new Email();

        email.setEmailAddress("gustavocalonico@edu.unirio.br");
        email.setMensagem("Teste de PM kappa, 123");

        return email;
    }
}
