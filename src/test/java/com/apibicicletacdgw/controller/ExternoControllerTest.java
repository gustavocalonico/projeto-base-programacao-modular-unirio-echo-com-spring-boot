package com.apibicicletacdgw.controller;

import com.apibicicletacdgw.dto.CartaoDeCreditoDto;
import com.apibicicletacdgw.dto.EmailDto;
import com.apibicicletacdgw.dto.NewCobrancaDto;
import com.apibicicletacdgw.model.CartaoDeCredito;
import com.apibicicletacdgw.model.Cobranca;
import com.apibicicletacdgw.model.Email;
import com.apibicicletacdgw.service.ICartaoService;
import com.apibicicletacdgw.service.IEmailService;
import com.apibicicletacdgw.service.IExternoService;
import org.apache.commons.mail.EmailException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static com.apibicicletacdgw.factory.ModelFactory.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class ExternoControllerTest {

    private ExternoController externoController;
    private IExternoService externoService;
    private IEmailService emailService;
    private ICartaoService cartaoService;
    private ModelMapper mapper;

    @BeforeEach
    void beforeTest() {

        externoService = mock(IExternoService.class);
        emailService = mock(IEmailService.class);
        cartaoService = mock(ICartaoService.class);
        mapper = new ModelMapper();

        externoController = new ExternoController(externoService, emailService, cartaoService, mapper);
    }

    @Test
    void testGetRoot() {
        assertEquals("Endpoint raiz externo das bicicletas.", externoController.getRoot());
    }

    @Test
    void testPostSendMail() throws EmailException {
        Email email = createEmail();
        EmailDto emailDto = mapper.map(email, EmailDto.class);

        when(emailService.send(email)).thenReturn(true);
        when(emailService.insertMail(email)).thenReturn(email);

        String emailResp = externoController.postSentMail(emailDto);

        verify(emailService, times(1)).send(email);
        verify(emailService, times(1)).insertMail(email);
        assertEquals("Externo solicitada", emailResp);
    }

    @Test
    void testPostCharge() {
        Cobranca cobranca = createCobranca();
        NewCobrancaDto cobrancaDto = mapper.map(cobranca, NewCobrancaDto.class);

        when(externoService.insertCobranca(cobranca)).thenReturn(cobranca);

        Cobranca cobrancaResp = externoController.postCharge(cobrancaDto);

        verify(externoService, times(1)).insertCobranca(cobranca);
        assertEquals(cobranca, cobrancaResp);
    }

    @Test
    void testPostChargeQueue() {
        Cobranca cobranca = createCobranca();
        NewCobrancaDto cobrancaDto = mapper.map(cobranca, NewCobrancaDto.class);

        when(externoService.insertOnCobrancaQueue(cobranca)).thenReturn(cobranca);

        Cobranca cobrancaResp = externoController.postChargeQueue(cobrancaDto);

        verify(externoService, times(1)).insertOnCobrancaQueue(cobranca);
        assertEquals(cobranca, cobrancaResp);
    }

    @Test
    void testGetChargeById() {
        UUID uuid = UUID.randomUUID();
        Cobranca cobranca = createCobranca();
        cobranca.setId(uuid);

        when(externoService.getCobrancaById(uuid)).thenReturn(cobranca);

        Cobranca cobrancaResp = externoController.getChargeById(uuid);

        verify(externoService, times(1)).getCobrancaById(uuid);
        assertEquals(uuid, cobrancaResp.getId());
        assertEquals(cobranca, cobrancaResp);

    }

    @Test
    void testPostValidateCreditCard() {
        CartaoDeCredito cartao = createCartao();
        CartaoDeCreditoDto cartaoDto = mapper.map(cartao, CartaoDeCreditoDto.class);

        when(cartaoService.creditOperatorValidation(cartao)).thenReturn(true);
        when(cartaoService.insertCartaoDeCredito(cartao)).thenReturn(cartao);

        assertEquals("Dados Atualizados", externoController.postValidateCreditCard(cartaoDto).getBody());
        verify(cartaoService, times(1)).creditOperatorValidation(cartao);
        verify(cartaoService, times(1)).insertCartaoDeCredito(cartao);
    }
}
