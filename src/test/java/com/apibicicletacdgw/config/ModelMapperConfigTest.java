package com.apibicicletacdgw.config;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ModelMapperConfigTest {
    @Test
    public void testModelMapper() {
        ModelMapperConfig modelMapperConfig = new ModelMapperConfig();
        assertNotNull(modelMapperConfig.modelMapper());
    }
}
